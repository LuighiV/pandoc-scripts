# Scripts for Pandoc conversion

These scripts have the purpose to convert files from one format to another,
employing pandoc as a tool for conversion.

Then, there are snippets of code which performs such conversion depending on
the case of use.

## Convert from HTML to Doc

To convert from HTML to DOC files, just use the script in this file an place
the HTML files in a folder named `html` and the word files will be created in 
a `word` folder. 
```bash
./convert_from_html_to_word.sh
```

