#/bin/bash -x

SOURCE_DIR=./html
SOURCE_IMAGES=$SOURCE_DIR/_static

mkdir -p word
# Based on
# https://stackoverflow.com/a/54563899/5107192
# To understand:
# https://unix.stackexchange.com/questions/209123/understanding-ifs-read-r-line
while IFS= read -r -d $'\0' file; do
  echo "Converting: " "$file " 
  BASENAME="`basename "$file"`"
  pandoc -s "$file" \
      --resource-path=$SOURCE_IMAGES \
      -o 'word/'"${BASENAME%.html}"'.docx'
done < <( find $SOURCE_DIR -type f -name "*.html" -print0)
